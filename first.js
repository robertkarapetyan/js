console.log("bibbiti bobbiti boo")

// tt = 66;
// console.log("tt defined as : " + tt);
// let a = globalThis.tt;
// console.log("tt is : " + a);

// let o = { x: 1, y: 2 }; // The object we'll loop over
// for (const [name, value] of Object.entries(o)) {
//     console.log(name, value); // Prints "x 1" and "y 2"
// }

let brbr = {};
//alert(Object.prototype.isPrototypeOf(brbr));
tt = 66;
//document.getElementById("ss").innerHTML = tt;


// let arr = [3, 3, 4, 2, 1];
// let car = {
//     door_count : 4,
//     color : "red"
// };

// let ttx = {};
// ttx = {77: "y"};

// function foo(){
//     var a = 55;
//     var a = 66;
//     console.log("a = " + a);
// }
//foo();

//  1.   ---------------------------------------------------------------------------
// serializing   JSON.stringify(a)      ->>>>call's a.toJSON() wich invoke toString()(optional)
// deserializing JSON.parse(str)
// let point = {
//     x: 1,
//     y: 2,
//     toString() { return `(${this.x}, ${this.y})`; },
//     toJSON: function () {
//         let jstr = "{ ";
//         for (let key of Object.keys(this)) {
//             if (typeof (this[key]) != "function") {
//                 jstr += `${key}:${this[key]}, `;
//             }
//         }
//         jstr += "}";

//         return jstr;
//     }
// };
// console.log(point.toString() + " : toString()");

// console.log(JSON.stringify(point));

// console.log(point.toJSON());

//  2.  ---------------------------------------------------------------------------
//                              object literals
// let z = 7, w = 8;
// let obj1 = {x:5, y:6};
// let obj2 = {x1:obj1.x, y1:obj1.y};
// console.log(obj2.x1);
// let obj3 = {z,w};
// console.log(obj3.z);

//  3.  ---------------------------------------------------------------------------
//                              getter&&setter
// let point = {
//     x:1,
//     y:2,

//     get x_by_y(){
//         return this.x*this.y;
//     },

//     get gt_x(){
//         console.log("getx");
//         return this.x;
//     },
//     set st_x(xval){
//         this.x = xval;
//     }
// }
// point.st_x = 25;
// let vv = point.g_x;
// let at = point.x_by_y;
// console.log(at);

//  4.  ---------------------------------------------------------------------------
//                          functions
// let f = function(x){return x*x;};
// console.log(f(4));

// let ff = function fact(x) { return (x <= 1) ? 1: x*fact(x-1);};
// console.log(function(xx){ return xx*xx;}(12));

// function max(x) {
//     let maxValue = -Infinity;
//     // Loop through the arguments, looking for, and remembering, the biggest.
//     for(let i = 0; i < arguments.length; i++) {
//     if (arguments[i] > maxValue) maxValue = arguments[i];
//     }
//     // Return the biggest
//     return maxValue;
// }
// console.log(max(1, 10, 100, 2, 3, 1000, 4, 5, 6)); // => 1000

//----------    \\\     8.4, 201 page       ///     -------------------------------Example 8 - 1. Using functions as data
//WTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTF
// We define some simple functions here
// function add(x, y) { return x + y; }
// function subtract(x, y) { return x - y; }
// function multiply(x, y) { return x * y; }
// function divide(x, y) { return x / y; }
// // Here's a function that takes one of the preceding functions
// // as an argument and invokes it on two operands
// function operate(operator, operand1, operand2) {
//     return operator(operand1, operand2);
// }
// //8.4 Functions as Values | 201
// // We could invoke this function like this to compute the value (2+3) + (4*5):
// let i = operate(add, operate(add, 2, 3), operate(multiply, 4, 5));
// // For the sake of the example, we implement the simple functions again,
// // this time within an object literal;
// const operators = {
//     add: (x, y) => x + y,
//     subtract: (x, y) => x - y,
//     multiply: (x, y) => x * y,
//     divide: (x, y) => x / y,
//     pow: Math.pow // This works for predefined functions too
// };
// // This function takes the name of an operator, looks up that operator
// // in the object, and then invokes it on the supplied operands. Note
// // the syntax used to invoke the operator function.
// function operate2(operation, operand1, operand2) {
//     if (typeof operators[operation] === "function") {
//         return operators[operation](operand1, operand2);
//     }
//     else throw "unknown operator";
// }
// operate2("add", "hello", operate2("add", " ", "world")) // => "hello world"
// operate2("pow", 10, 2) // => 100
//WTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTFWTF

// function timed(f) {
//     //return function(...args) { // Collect args into a rest parameter array
//     console.log(`Entering function ${f.name}`);
//     //return 55;
//     //}
// }
// timed();

// getting random number in specified range
// function randInRange(min, max){
//     return Math.round(Math.random() * (max - min) + min);
// }

// for(let i = 0; i < 500; ++i){
//     let a = randInRange(10, 42);
//     console.log(a);
// }

//  5.  ---------------------------------------------------------------------------
//  manipulating arrays of objects

// let arr = [
//     {name: "Garry", surname:"Potter"},
//     {name: "John", surname:"Doe"},
//     {name: "Tom", surname: "Sawyer"}
// ]

// let index = arr.findIndex(function(someObj){
//     return someObj.name.toLowerCase() == "john";
// })
// let person = arr.find(person => person.name == "Tom")
// console.log(index);
// console.log(person);

// const {name: abfd} = person;
// console.log("abfd destructured from person is : " + abfd);

// let obj1 = {x:5, y:6};
// let obj2 = {x1:obj1.x, y1:obj1.y};

//  6.  ----------------------------------------------------------------------------
//logger

let point = {
    _x:1,
    _y:2,
    // x_by_y(){
    //     return this._x*this._y;
    // },
    static originDistance(x,y){
        return Math.hypot(x, y);
    },

    get x(){
        console.log("getx");
        return this._x;
    },
    set x(xval){
        this._x = xval;
    }
}

// const logger = {
//     logKeys(obj){ console.log(Object.keys(obj))},
//     logKeysAndValues(obj){ Object.keys(obj).forEach(element => {
//         console.log(`${element} : ${obj[element]}`)
//     });}
// }

// const loggerWTF = {
//     logKeysWTF(){ console.log(Object.keys(this))}
// }

// console.log("   Object Point's keys logged");
// logger.logKeys(point); 
// loggerWTF.logKeysWTF.call(point)

// console.log("   Object Point's keys and values logged");
// logger.logKeysAndValues(point);

// // arrow vs traditional functions

// point.arrow = (obj) => Object.keys(this);
// point.traditional = function(obj){return Object.keys(this);}


// console.log(" arrow() : ");
// console.log( point.arrow(point));
// console.log("traditional() : ")
// console.log(point.traditional(point))

//  7   -----------------------------------------------------------------------
//  shape abstraction 
//        shape
//    /     |     \
// rect  triangle  circle

// class Shape{
//     constructor(c){this._color = c}
//     set color(color){this._color = color}
//     get color(){return this._color}
//     hasGeometry(){
//             throw new Error("abstract method called");
//     }
// }

// class Rectangle extends Shape{
//     constructor(w, h, c){
//         super(c);
//         this._width = w; 
//         this._height = h; 
//     }

//     set height(h) {this._height = h}
//     set width(w) {this._width = w}

//     get height() {return this._height}
//     get width() {return this._width}

//     get perimeter() {return (this.width + this.height)*2}
//     get surface() {return this.width * this.height};
//     hasGeometry(){
//         return true;
//     }
// }

// class Circle extends Shape{
//     constructor(r, c){
//         super(c);
//         this._radius = r; 
//     }
//     set radius(r) { this._radius = r}
//     get radius() { return this._radius}

//     get surface(){ return ((Math.PI * this.radius)**2).toPrecision(4)}
//     get circumference(){ return (Math.PI*2*this.radius).toPrecision(4)}
//     hasGeometry(){
//         return true;
//     }
// }

// let shp = new Shape;

// let a = new Rectangle(21,2,"red");
// // a.width = 21;
// // a.height = 2;
// console.log(a.surface);
// console.log(a.perimeter);
// // console.log(a.hasGeometry());


// let cr = new Circle(2, "green");
// //cr.radius = 2;
// console.log(`circle surface with radius ${cr.radius} is : ${cr.surface}`);
// console.log(`circle circumference with radius ${cr.radius} is : ${cr.circumference}`);
